//
//  CachedTemp+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedTemp {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedTemp> {
        return NSFetchRequest<CachedTemp>(entityName: "CachedTemp")
    }

    @NSManaged public var unit: String
    @NSManaged public var value: Double
    @NSManaged public var originFermentation: NSSet?
    @NSManaged public var originMashTemp: NSSet?

}

// MARK: Generated accessors for originFermentation
extension CachedTemp {

    @objc(addOriginFermentationObject:)
    @NSManaged public func addToOriginFermentation(_ value: CachedFermentation)

    @objc(removeOriginFermentationObject:)
    @NSManaged public func removeFromOriginFermentation(_ value: CachedFermentation)

    @objc(addOriginFermentation:)
    @NSManaged public func addToOriginFermentation(_ values: NSSet)

    @objc(removeOriginFermentation:)
    @NSManaged public func removeFromOriginFermentation(_ values: NSSet)

}

// MARK: Generated accessors for originMashTemp
extension CachedTemp {

    @objc(addOriginMashTempObject:)
    @NSManaged public func addToOriginMashTemp(_ value: CachedMashTemp)

    @objc(removeOriginMashTempObject:)
    @NSManaged public func removeFromOriginMashTemp(_ value: CachedMashTemp)

    @objc(addOriginMashTemp:)
    @NSManaged public func addToOriginMashTemp(_ values: NSSet)

    @objc(removeOriginMashTemp:)
    @NSManaged public func removeFromOriginMashTemp(_ values: NSSet)

}

extension CachedTemp : Identifiable {

}
