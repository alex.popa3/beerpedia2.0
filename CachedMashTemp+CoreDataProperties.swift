//
//  CachedMashTemp+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedMashTemp {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedMashTemp> {
        return NSFetchRequest<CachedMashTemp>(entityName: "CachedMashTemp")
    }

    @NSManaged public var duration: Int16
    @NSManaged public var originMethod: NSSet
    @NSManaged public var temp: CachedTemp

}

// MARK: Generated accessors for originMethod
extension CachedMashTemp {

    @objc(addOriginMethodObject:)
    @NSManaged public func addToOriginMethod(_ value: CachedMethod)

    @objc(removeOriginMethodObject:)
    @NSManaged public func removeFromOriginMethod(_ value: CachedMethod)

    @objc(addOriginMethod:)
    @NSManaged public func addToOriginMethod(_ values: NSSet)

    @objc(removeOriginMethod:)
    @NSManaged public func removeFromOriginMethod(_ values: NSSet)

}

extension CachedMashTemp : Identifiable {

}
