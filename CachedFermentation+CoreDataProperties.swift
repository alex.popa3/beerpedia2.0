//
//  CachedFermentation+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 28/04/23.
//
//

import Foundation
import CoreData


extension CachedFermentation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedFermentation> {
        return NSFetchRequest<CachedFermentation>(entityName: "CachedFermentation")
    }

    @NSManaged public var originMethod: NSSet?
    @NSManaged public var temp: CachedTemp

}

// MARK: Generated accessors for originMethod
extension CachedFermentation {

    @objc(addOriginMethodObject:)
    @NSManaged public func addToOriginMethod(_ value: CachedMethod)

    @objc(removeOriginMethodObject:)
    @NSManaged public func removeFromOriginMethod(_ value: CachedMethod)

    @objc(addOriginMethod:)
    @NSManaged public func addToOriginMethod(_ values: NSSet)

    @objc(removeOriginMethod:)
    @NSManaged public func removeFromOriginMethod(_ values: NSSet)

}

extension CachedFermentation : Identifiable {

}
