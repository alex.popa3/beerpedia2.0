//
//  ErrorAlert.swift
//  BeerPedia
//
//  Created by Rawfish on 02/05/23.
//

import SwiftUI

struct ErrorAlert: View {
    let title: String
    let description: String
    let action: () -> Void
    
    @EnvironmentObject var viewModel: FavoriteViewModel
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack {
            Text(description)
                .frame(maxWidth: .infinity)
                .ignoresSafeArea()
                .font(.system(size: 14))
                .fontWeight(.medium)
                
            Button(title, action: action)
                .foregroundColor(.red)
                .fontWeight(.medium)
                .padding(10)
                .padding([.leading, .trailing], 41)
                .background(.red.opacity(0.30))
                .cornerRadius(30)
        }
        .frame(maxWidth: .infinity)
        .buttonStyle(.plain)
        .padding()
    }
}
