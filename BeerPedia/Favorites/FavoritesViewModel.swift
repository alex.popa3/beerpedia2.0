//
//  FavoritesViewModel.swift
//  BeerPedia
//
//  Created by Rawfish on 27/04/23.
//

import CoreData
import Foundation

class FavoriteViewModel: ObservableObject {
    
    @Published var data: [CachedBeer] = []
    @Published var showDeleteAlert = false
    @Published var showAllDeleteAlert = false
    @Published var selectedBeer: CachedBeer? = nil
    
    let container = NSPersistentContainer(name: "Model")

    static let shared = FavoriteViewModel()
    
    init() {
        //Container // DB loading
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                    if let error = error as NSError? {
                    print("Unresolved error \(error), \(error.userInfo)")
                    } else {
                        
                        print(storeDescription)
                    }
                })
        
        // Used to avoid duplicates in the context
        self.container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        
        
    }
    
    func fetchData() {
        let request = NSFetchRequest<CachedBeer>(entityName: "CachedBeer")
        do {
            data = try container.viewContext.fetch(request)
        } catch {
            print("Error fetching. \(error)")
        }
    }
    
    func addData(beer: BeerElement) {
        let entity = CachedBeer(context: container.viewContext)
        entity.id = Int16(beer.id)
        entity.name = beer.name
        entity.tagline = beer.tagline
        entity.first_brewed = beer.first_brewed
        entity.beer_description = beer.description
        entity.image = beer.unwrappedImage
        entity.abv = beer.unwrappedAbv
        entity.ibu = beer.unwrappedIbu
        entity.target_fg = beer.unwrappedTargetFG
        entity.target_og = beer.unwrappedTargetOg
        entity.ebc = beer.unwrappedEbc
        entity.srm = beer.unwrappedSrm
        entity.ph = beer.unwrappedPh
        entity.attenuation_level = beer.unwrappedAttenuationLevel
        let volume = CachedVolume(context: container.viewContext)
        volume.value = beer.unwrappedVolume.unwrappedValue
        volume.unit = beer.unwrappedVolume.unwrappedUnit
        entity.volume = volume
        let boilVolume = CachedVolume(context: container.viewContext)
        boilVolume.value = beer.unwrappedBoilVolume.unwrappedValue
        boilVolume.unit = beer.unwrappedBoilVolume.unwrappedUnit
        entity.boilVolume = boilVolume
        let cachedIngredients = CachedIngredients(context: container.viewContext)
        
        // Adding Hops
        for hop in beer.unwrappedIngredients.hops {
            
            let ingredientHops = CachedHops(context: container.viewContext)
            ingredientHops.name = hop.unwrappedName
            
            let amount = CachedAmount(context: container.viewContext)
            amount.unit = hop.unwrappedAmount.unwrappedUnit
            amount.value = hop.unwrappedAmount.unwrappedValue
            
            ingredientHops.amount = amount
            
            cachedIngredients.addToHops(ingredientHops)
            
        }
        
        // Adding Malts
        for malt in beer.unwrappedIngredients.malt {
            
            let cachedMalt = CachedMalt(context: container.viewContext)
            cachedMalt.name = malt.unwrappedName
            
            let amount = CachedAmount(context: container.viewContext)
            amount.unit = malt.unwrappedName
            amount.value = malt.unwrappedAmount.unwrappedValue
            
            cachedMalt.amount = amount
            
            cachedIngredients.addToMalt(cachedMalt)
           
        }
        
        let cachedMethod = CachedMethod(context: container.viewContext)
            
        for mashTemp in beer.unwrappedMethod.mash_temp {
            let mash = CachedMashTemp(context: container.viewContext)
            mash.duration = Int16(mashTemp.unwrappedDuration)
                
            let tempCached = CachedTemp(context: container.viewContext)
            tempCached.unit = mashTemp.temp.unwrappedUnit
            tempCached.value = mashTemp.temp.unwrapppedValue
                
            mash.temp = tempCached
            cachedMethod.addToMash_temp(mash)
        }
            
        let cachedFermentation = CachedFermentation(context: container.viewContext)
        let temp = CachedTemp(context: container.viewContext)
        temp.value = beer.unwrappedMethod.unwrappedFermentation.temp.unwrapppedValue
        temp.unit = beer.unwrappedMethod.unwrappedFermentation.temp.unwrappedUnit
        cachedFermentation.temp = temp
        cachedMethod.fermentation = cachedFermentation
            
        entity.ingredients = cachedIngredients
        entity.method = cachedMethod
        entity.foodpairing = beer.unwrappedFoodPairing
        entity.brewers_tips = beer.unwrappedBrewerTips
        entity.contributed_by = beer.unwrappedcontributedBy
        entity.yeast = beer.unwrappedIngredients.yeast!
        entity.favorite = true
        
        print(entity.foodpairing)
        saveContext()
        fetchData()
    }
    
    func saveContext() {
        do {
            try container.viewContext.save()
            
        } catch let error as NSError {
            print("Error: \(error), \(error.userInfo)")
        }
    }
    
    func deleteBeer() {
        print("Eliminazione avviata")
        
            if let selectedBeer = selectedBeer {
                container.viewContext.delete(selectedBeer)
                data.removeAll { selectedBeer.id == $0.id }
                self.selectedBeer = nil
                saveContext()
                showDeleteAlert = false
                fetchData()
            } else {
                print("Error to remove the beer from favorites")
        }
    }
    
    func deleteAllData() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = CachedBeer.fetchRequest()
        let batchDelete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        _ = try? container.viewContext.execute(batchDelete)
        data.removeAll()
        showAllDeleteAlert = false
        
    }
}

extension BeerElement {
    init?(cachedBeer: CachedBeer) {
        var fermentationInstance: Fermentation {
            let value = cachedBeer.method.fermentation.temp.value
            let unit = cachedBeer.method.fermentation.temp.unit
            
            return Fermentation(temp: Temp(value: value, unit: unit))
        }
        var mashTempInstance: Mash_temp {
            var value = 0.0
            var unit = ""
            var duration = 0
            
            cachedBeer.method.mashTemps.map { mash in
                value = mash.temp.value
                unit = mash.temp.unit
                duration = Int(mash.duration)
            }
            
            return Mash_temp(temp: Temp(value: value, unit: unit), duration: duration)
        }
        
        var hopIstance: [Hop] {
            var dataArray: [Hop] = []
                
            cachedBeer.ingredients.hopArray.map { hop in
                    
                let hop = Hop(name: hop.name, amount: Amount(value: hop.amount.value, unit: hop.amount.unit), add: "", attribute: "")
                dataArray.append(hop)
            }
                
            return dataArray
        }
        var maltIstance: [Malt] {
            var dataArray: [Malt] = []

            cachedBeer.ingredients.maltArray.map { malt in
                    
                let malt = Malt(name: malt.name, amount: Amount(value: malt.amount.value, unit: malt.amount.unit))
                dataArray.append(malt)
            }
                
            return dataArray
        }
            
        self.init(id: Int(cachedBeer.id),
                  name: cachedBeer.name,
                  tagline: cachedBeer.tagline,
                  first_brewed: cachedBeer.first_brewed,
                  description: cachedBeer.beer_description,
                  image_url: cachedBeer.image,
                  abv: cachedBeer.abv,
                  ibu: cachedBeer.ibu,
                  target_fg: cachedBeer.target_fg,
                  target_og: cachedBeer.target_og,
                  ebc: cachedBeer.ebc,
                  srm: cachedBeer.srm,
                  ph: cachedBeer.ph,
                  attenuation_level: cachedBeer.attenuation_level,
                  volume: Boil_Volume(value: cachedBeer.volume.value, unit: cachedBeer.volume.unit),
                  boil_volume: Boil_Volume(value: cachedBeer.boilVolume.value, unit: cachedBeer.boilVolume.unit),
                  method: Method(mash_temp: [mashTempInstance], fermentation: fermentationInstance),
                  ingredients: Ingredients(malt: maltIstance, hops: hopIstance, yeast: ""),
                  food_pairing: cachedBeer.foodpairing,
                  brewers_tips: cachedBeer.brewers_tips,
                  contributed_by: cachedBeer.contributed_by
        )
    }
}
