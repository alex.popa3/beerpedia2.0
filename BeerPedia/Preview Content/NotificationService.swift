//
//  NotificationService.swift
//  BeerPedia
//
//  Created by Rawfish on 20/04/23.
//

import Foundation
import UserNotifications
import Combine


class NotificationsService {
    
    let notifications = UNUserNotificationCenter.current()
    private let notificationSubject : PassthroughSubject<NSNotification,Error>
    let notificationPublisher: AnyPublisher<NSNotification,Error>
    
    init() {
        notificationSubject = PassthroughSubject<NSNotification,Error>()
        notificationPublisher = notificationSubject.eraseToAnyPublisher()
    }
    
    
    func updatePermissionStatus() -> String  {
        var value: String = ""
        notifications.getNotificationSettings { status  in
           
            if status.authorizationStatus == .denied {
                 value = "Negata"
            } else if status.authorizationStatus == .notDetermined {
                value = "Non determinato"
            } else if status.authorizationStatus == .authorized {
                value = "Approved"
            }
           
        }
        return value
    }
}
