//
//  DetailViewSheets.swift
//  BeerPedia
//
//  Created by Rawfish on 13/04/23.
//

import SwiftUI

struct BeerDetailSheets: View {
    @EnvironmentObject var viewModel: ViewModel
    let beer: BeerElement
    var body: some View {
        ZStack {
            ScrollView(showsIndicators: false) {
                LazyVStack(alignment: .leading) {
                    IngredientsDialog(beer: beer)
                        .environmentObject(viewModel)
                    MethodDialog(beer: beer)
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .beerTheme()
    }
}

struct BeerValuesSheet: View {
    @EnvironmentObject var viewModel: ViewModel
    let beer: BeerElement
    var body: some View {
        ZStack {
            ScrollView {
                VStack {
                    BeerValueCell(value: beer.formattedAbv, title: "Abv")
                    BeerValueCell(value: beer.formattedIbu, title: "Ibu")
                    BeerValueCell(value: beer.formattedTargetFg, title: "Target Fg")
                    BeerValueCell(value: beer.formattedTargetOg, title: "Target Og")
                    BeerValueCell(value: beer.formattedEbc, title: "Ebc")
                    BeerValueCell(value: beer.formattedSrm, title: "Srm")
                    BeerValueCell(value: beer.formattedPh, title: "Ph")
                    BeerValueCell(value: beer.formattedAttenuationLevel, title: "AttenuationLevel")
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .beerTheme()
    }
}

struct DetailViewSheets_Previews: PreviewProvider {
    static var previews: some View {
        BeerValuesSheet(beer: BeerElement.MockedBeer)
            .environmentObject(ViewModel())
    }
}
