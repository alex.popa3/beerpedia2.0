//
//  BundleExtension.swift
//  BeerPedia
//
//  Created by Rawfish on 03/05/23.
//

import Foundation


extension Bundle {
    var appName: String {
        return object(forInfoDictionaryKey: "CFBundleName") as! String
    }
    var appVersion: String {
        return object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    var build: String {
        return object(forInfoDictionaryKey: "CFBundleVersion") as! String
    }
}
