//
//  BeerDetailWidgets.swift
//  BeerPedia
//
//  Created by Rawfish on 03/05/23.
//

import Foundation
import SwiftUI

struct hopWidget: View {
    let beer: BeerElement
    
    var body: some View {
        Text("Hops")
            .font(.title3.bold())
            .frame(maxWidth: .infinity)
            .padding(5)
            .background(.regularMaterial)
            .cornerRadius(30)
            .padding([.leading, .trailing], 15)
        
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(beer.unwrappedIngredients.hops, id: \.self) { hop in
                    HStack {
                        Text("\(hop.unwrappedName)")
                            .padding()
                            .background(.regularMaterial)
                            .cornerRadius(20)
                        HStack {
                            Text("\(hop.unwrappedAmount.formattedValue) \(hop.unwrappedAmount.unwrappedUnit)")
                                .fontWeight(.light)
                           
                        }
                        .padding()
                    }
                    
                    .padding(10)
                    .background(.regularMaterial)
                    .cornerRadius(30)
                }
            }
            .padding(.bottom, 15)
            .padding([.leading, .trailing], 15)
        }
    }
}
                                
struct MaltWidget: View {
    let beer: BeerElement
    
    var body: some View {
        Text("Malt")
            .font(.title3.bold())
            .frame(maxWidth: .infinity)
            .padding(5)
            .background(.regularMaterial)
            .cornerRadius(30)
            .padding([.leading, .trailing], 15)
            .padding(.top, 15)
        
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                ForEach(beer.unwrappedIngredients.malt, id: \.self) { malt in
                    HStack {
                        Text(malt.unwrappedName)
                            .padding()
                            .background(.regularMaterial)
                            .cornerRadius(20)
                        HStack {
                            Text("\(malt.unwrappedAmount.formattedValue) \(malt.unwrappedAmount.unwrappedUnit) ")
                                .fontWeight(.light)
                        }
                    }
                    .padding(10)
                    .background(.regularMaterial)
                    .cornerRadius(30)
                }
            }
            .padding(.bottom, 2.5)
            .padding([.leading, .trailing], 15)
        }
    }
}


struct MethodMashTempWidget: View {
    let beer: BeerElement
    var body: some View {
        
        Text("Mash Temperature")
            .font(.title3.bold())
            .frame(maxWidth: .infinity)
            .padding(5)
            .background(.regularMaterial)
            .cornerRadius(30)
            .padding([.leading, .trailing], 15)
            .padding(.top, 15)
        
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                ForEach(beer.unwrappedMethod.mash_temp, id: \.self) { mash in
                    HStack {
                        Text("Duration: \(mash.unwrappedDuration)")
                            .fontWeight(.semibold)
                            .padding()
                            .background(.regularMaterial)
                            .cornerRadius(20)
                        Text("\(mash.temp.formattedValue) \(mash.temp.unwrappedUnit) ")
                            .fontWeight(.light)
                       
                        HStack {
                            //
                        }
                    }
                    .padding(10)
                    .background(.regularMaterial)
                    .cornerRadius(30)
                }
            }
            .padding(.bottom, 2.5)
            .padding([.leading, .trailing], 15)
        }
    }
}

struct MethodFermentationWidget: View {
    let beer: BeerElement
    
    var body: some View {
        Text("Fermentation")
            .font(.title3.bold())
            .frame(maxWidth: .infinity)
            .padding(5)
            .background(.regularMaterial)
            .cornerRadius(30)
            .padding([.leading, .trailing], 15)
        
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                HStack {
                    Text("\(beer.unwrappedMethod.unwrappedFermentation.temp.formattedValue) \(beer.unwrappedMethod.unwrappedFermentation.temp.unwrappedUnit)")
                        .fontWeight(.light)
                }
                .padding()
                    
                .padding(10)
                .background(.regularMaterial)
                .cornerRadius(30)
            }
        }
        .padding(.bottom, 15)
        .padding([.leading, .trailing], 15)
    }
}

/*struct Widget: View {
    @Binding var beer: BeerElement
    let data: [Any] = []
    let data1 : String = ""
    let data2: String = ""
    let data3: String? = nil
    var body: some View {
        Text("Mash Temperature")
            .font(.title3.bold())
            .frame(maxWidth: .infinity)
            .padding(5)
            .background(.regularMaterial)
            .cornerRadius(30)
            .padding([.leading, .trailing], 15)
            .padding(.top, 15)
        
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                
                ForEach(beer.unwrappedFoodPairing, id: \.self) { _ in
                    HStack {
                        Text("Duration: \(data1)")
                            .fontWeight(.semibold)
                            .padding()
                            .background(.regularMaterial)
                            .cornerRadius(20)
                        Text("\(data2) \(data3) ")
                            .fontWeight(.light)
                       
                        HStack {
                            //
                        }
                    }
                    .padding(10)
                    .background(.regularMaterial)
                    .cornerRadius(30)
                }
            }
            .padding(.bottom, 2.5)
            .padding([.leading, .trailing], 15)
        }
    }
}*/
