//
//  BeerDetailViewDialogs.swift
//  BeerPedia
//
//  Created by Rawfish on 03/05/23.
//

import Foundation
import SwiftUI

struct MethodDialog: View {
    let beer: BeerElement
    
    var body: some View {
        Text("Method")
            .font(.largeTitle.bold())
            .offset(x: 20, y: 15)
        LazyVStack {
           MethodMashTempWidget(beer: beer)
            MethodFermentationWidget(beer: beer)
            
          
        }
        .background(.regularMaterial.opacity(0.50))
        .cornerRadius(30)
        .padding([.leading, .trailing], 10)
    }
}



struct IngredientsDialog: View {
    @EnvironmentObject var viewModel: ViewModel
    let beer: BeerElement
    
    var body: some View {
        Text("Ingredients")
            .font(.largeTitle.bold())
            .offset(x: 20, y: 15)
        LazyVStack {
            MaltWidget(beer: beer)
            hopWidget(beer: beer)
        }
        .background(.regularMaterial.opacity(0.50))
        .cornerRadius(30)
        .padding([.leading, .trailing], 10)
    }
}




