//
//  EmptyFavoritesList.swift
//  BeerPedia
//
//  Created by Rawfish on 04/05/23.
//

import SwiftUI

struct EmptyFavoritesList: View {
    var body: some View {
        ZStack {
            Image(systemName: "mug")
                .resizable()
                .frame(width: 100, height: 100)
                .foregroundColor(.label)
            Image(systemName: "x.circle.fill")
                .resizable()
                .frame(width: 40, height: 40)
                .offset(x: 30, y: 30)
                .foregroundStyle(.regularMaterial, .black, .red.opacity(1.0))
        }
        .offset(x: 8)
        .padding([.top, .bottom], 10)
        Text("No favorites beer saved!")
            .fontWeight(.semibold)
            .padding([.top, .bottom], 7.5)
    }
}

struct EmptyFavoritesList_Previews: PreviewProvider {
    static var previews: some View {
        EmptyFavoritesList()
    }
}
