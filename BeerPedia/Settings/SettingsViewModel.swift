//
//  SettingsViewModel.swift
//  BeerPedia
//
//  Created by Rawfish on 21/04/23.
//

import Foundation

class SettingsViewModel: ObservableObject {
    @Published var loadedBeers: Int = 10 {
        didSet { Api.loadedBeerPeerPageSubject.send(self.loadedBeers) }
    }
    
    let authViewModel = AuthManager()
    
     
    
    
    @Published var infoDialog = false
    @Published var termsAndConditions = false
    @Published var logout = false {
        didSet {AuthService.logoutProcess.send(self.logout)}
    }
}
