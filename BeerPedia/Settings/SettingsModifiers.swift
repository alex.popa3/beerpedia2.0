//
//  SettingsModifiers.swift
//  BeerPedia
//
//  Created by Rawfish on 21/04/23.
//

import Foundation
import SwiftUI


struct SettingsCellModifier: ViewModifier {
    let settingTitle: String
    func body(content: Content) -> some View {
        content
            .frame(height: 25)
            .frame(maxWidth: .infinity)
            .padding()
            .background( settingTitle == "Logout" ? AnyShapeStyle(.red) : AnyShapeStyle(.regularMaterial))
            .cornerRadius(30)
            .padding([.leading,.trailing])
    }
}


extension View {
    
    func settingsCellModifier(settingTitle: String) -> some View {
        modifier(SettingsCellModifier(settingTitle: settingTitle))
    }
}
