//
//  CachedBeer+CoreDataProperties.swift
//  BeerPedia
//
//  Created by Rawfish on 03/05/23.
//
//

import Foundation
import CoreData


extension CachedBeer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedBeer> {
        return NSFetchRequest<CachedBeer>(entityName: "CachedBeer")
    }

    @NSManaged public var abv: Double
    @NSManaged public var attenuation_level: Double
    @NSManaged public var beer_description: String
    @NSManaged public var brewers_tips: String
    @NSManaged public var contributed_by: String
    @NSManaged public var ebc: Double
    @NSManaged public var favorite: Bool
    @NSManaged public var first_brewed: String
    @NSManaged public var foodpairing: [String]
    @NSManaged public var ibu: Double
    @NSManaged public var id: Int16
    @NSManaged public var image: String
    @NSManaged public var name: String
    @NSManaged public var ph: Double
    @NSManaged public var srm: Double
    @NSManaged public var tagline: String
    @NSManaged public var target_fg: Double
    @NSManaged public var target_og: Double
    @NSManaged public var yeast: String
    @NSManaged public var boilVolume: CachedVolume
    @NSManaged public var ingredients: CachedIngredients
    @NSManaged public var method: CachedMethod
    @NSManaged public var volume: CachedVolume
 
    

}

// MARK: Generated accessors for ingredients
extension CachedBeer {

    @objc(addIngredientsObject:)
    @NSManaged public func addToIngredients(_ value: CachedIngredients)

    @objc(removeIngredientsObject:)
    @NSManaged public func removeFromIngredients(_ value: CachedIngredients)

    @objc(addIngredients:)
    @NSManaged public func addToIngredients(_ values: NSSet)

    @objc(removeIngredients:)
    @NSManaged public func removeFromIngredients(_ values: NSSet)

}

extension CachedBeer : Identifiable {

}
